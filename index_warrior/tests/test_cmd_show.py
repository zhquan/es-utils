#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2018 Bitergia
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Authors:
#     Luis Cañas-Díaz <lcanas@bitergia.com>
#

import httpretty
import elasticsearch as es
import io
import os
import sys
import unittest

if '..' not in sys.path:
    sys.path.insert(0, '..')

from index_warrior import IndexWarrior

def read_file(filename, mode='r'):
    """Adhoc function to make reading files easier"""
    with open(os.path.join(os.path.dirname(os.path.abspath(__file__)), filename), mode) as fdesc:
        content = fdesc.read()
        return content

class TestCommandShow(unittest.TestCase):
    """Test index command"""

    @httpretty.activate
    def test_show_index(self):
        """ Test subcommand index show """
        body_cat_indices = read_file('data/cat_indices.json')
        body_cat_aliases = read_file('data/cat_aliases.json')
        valid_command_output = read_file('data/output_indices_with_less.txt')

        httpretty.register_uri(httpretty.GET,
                               "http://iwarrior.biterg.io:9200/_cat/indices?format=json",
                               body=body_cat_indices,
                               content_type="application/json")

        httpretty.register_uri(httpretty.GET,
                               "http://iwarrior.biterg.io:9200/_cat/aliases?format=json",
                               body=body_cat_aliases,
                               content_type="application/json")

        client = es.Elasticsearch("http://iwarrior.biterg.io", timeout=30)

        # we do the following to get the standard output and compare it
        iwarrior = IndexWarrior(client)
        captured_output = io.StringIO()                 # Create StringIO object
        sys.stdout = captured_output                    #  and redirect stdout.
        iwarrior.show_indices(True)                     # Call function.
        sys.stdout = sys.__stdout__                     # Reset redirect

        self.assertEqual(captured_output.getvalue(), valid_command_output)
